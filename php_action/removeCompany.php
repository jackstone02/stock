<?php 	

require_once 'core2.php';


$valid['success'] = array('success' => false, 'messages' => array());

$companyId = $_POST['companyId'];

if($companyId) { 

	// $sql = "UPDATE brands SET brand_status = 2 WHERE brand_id = {$brandId}";
	$sql = "DELETE from Company where company_id =  {$companyId}";

	if($connect->query($sql) === TRUE) {
 		$valid['success'] = true;
		$valid['messages'] = "Successfully Removed";		
 	} else {
 		$valid['success'] = false;
 		$valid['messages'] = "Error while remove the brand";
 	}
 
 	$connect->close();

 	echo json_encode($valid);
 
} // /if $_POST