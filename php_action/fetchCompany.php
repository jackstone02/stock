<?php 	

require_once 'core2.php';

// $sql = "SELECT brand_id, brand_name, brand_active, brand_status FROM brands WHERE brand_status = 1";
// $sql = "SELECT company_id, name, companylogo FROM Company";
$sql = "SELECT c.company_id, c.name, c.companylogo, u.fname FROM Company c LEFT JOIN User u ON c.company_id = u.company_fk";
$result = $connect->query($sql);

$output = array('data' => array());

if($result->num_rows > 0) { 

 // $row = $result->fetch_array();
 $activeCompanies = ""; 

 while($row = $result->fetch_array()) {
 	$companyId = $row[0];
 	// active 
 	if($row[2] == 1) {
 		// activate member
 		$activeCompanies = "<label class='label label-success'>Available</label>";
 	} else {
 		// deactivate member
 		$activeCompanies = "<label class='label label-danger'>Not Available</label>";
 	}

 	// $link = '<a href="users.php?company_id='.$row[0].'" style="color: inherit">'.$row[1].'</a>';
 	if( !empty($row[3]) ) $display = 'display:none';
 		else $display = '';

 	$button = '<!-- Single button -->
	<div class="btn-group">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    	Action <span class="caret"></span>
	  	</button>
	  	<ul class="dropdown-menu" style="cursor:pointer">
	    	<li><a type="button" data-toggle="modal" data-target="#editCompanyModel" onclick="editCompanies('.$companyId.')"> <i class="glyphicon glyphicon-edit"></i> Edit</a></li>
	    	<li><a type="button" data-toggle="modal" data-target="#removeMemberModal" onclick="removeCompanies('.$companyId.')"> <i class="glyphicon glyphicon-trash"></i> Remove</a></li>
	    	<li style="'.$display.'"><a type="button" data-toggle="modal" data-target="#addUserModel_'.$companyId.'" onclick="addUser('.$companyId.')"> <i class="glyphicon glyphicon-plus-sign"></i> Add User</a></li>
	    	<li><a type="button" data-toggle="modal" data-target="#removeMemberModal" onclick="editUser('.$companyId.')"> <i class="glyphicon glyphicon-edit"></i> Edit User</a></li>
	  	</ul>
	</div>';

 	$output['data'][] = array( 		
 		$row[1],
 		$row[2],
 		// $activeCompanies,
 		$row[3],
 		$button
 		); 	
 } // /while 

} // if num_rows

$connect->close();

echo json_encode($output);