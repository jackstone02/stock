var manageCompanyTable;

$(document).ready(function() {
	// top bar active
	$('#navCompany').addClass('active');
	
	// manage company table
	manageCompanyTable = $("#manageCompanyTable").DataTable({
		'ajax': 'php_action/fetchCompany.php',
		'order': []		
	});

	// submit company form function
	$("#submitCompanyForm").unbind('submit').bind('submit', function() {
		// remove the error text
		$(".text-danger").remove();
		// remove the form error
		$('.form-group').removeClass('has-error').removeClass('has-success');			

		var companyName = $("#companyName").val();

		if(companyName == "") {
			$("#companyName").after('<p class="text-danger">Company Name field is required</p>');
			$('#companyName').closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#companyName").find('.text-danger').remove();
			// success out for form 
			$("#companyName").closest('.form-group').addClass('has-success');	  	
		}

		if(companyName) {
			var form = $(this);
			// button loading
			$("#createCompanyBtn").button('loading');

			$.ajax({
				url : form.attr('action'),
				type: form.attr('method'),
				data: form.serialize(),
				dataType: 'json',
				success:function(response) {
					// button loading
					$("#createCompanyBtn").button('reset');

					if(response.success == true) {
						// reload the manage member table 
						manageCompanyTable.ajax.reload(null, false);						

  	  			// reset the form text
						$("#submitCompanyForm")[0].reset();
						// remove the error text
						$(".text-danger").remove();
						// remove the form error
						$('.form-group').removeClass('has-error').removeClass('has-success');
  	  			
  	  			$('#add-company-messages').html('<div class="alert alert-success">'+
            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
          '</div>');

  	  			$(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					}  // if

				} // /success
			}); // /ajax	
		} // if

		return false;
	}); // /submit company form function

});

function editCompanies(companyId = null) {
	if(companyId) {
		// remove hidden company id text
		$('#companyId').remove();

		// remove the error 
		$('.text-danger').remove();
		// remove the form-error
		$('.form-group').removeClass('has-error').removeClass('has-success');

		// modal loading
		$('.modal-loading').removeClass('div-hide');
		// modal result
		$('.edit-company-result').addClass('div-hide');
		// modal footer
		$('.editCompanyFooter').addClass('div-hide');

		$.ajax({
			url: 'php_action/fetchSelectedCompany.php',
			type: 'post',
			data: {companyId : companyId},
			dataType: 'json',
			success:function(response) {
				// modal loading
				$('.modal-loading').addClass('div-hide');
				// modal result
				$('.edit-company-result').removeClass('div-hide');
				// modal footer
				$('.editCompanyFooter').removeClass('div-hide');

				// setting the company name value 
				$('#editCompanyName').val(response.name);
				// setting the company status value
				// $('#editCompaniestatus').val(response.company_active);
				// company id 
				$(".editCompanyFooter").after('<input type="hidden" name="companyId" id="companyId" value="'+response.company_id+'" />');

				// update company form 
				$('#editCompanyForm').unbind('submit').bind('submit', function() {

					// remove the error text
					$(".text-danger").remove();
					// remove the form error
					$('.form-group').removeClass('has-error').removeClass('has-success');			

					var companyName = $('#editCompanyName').val();
					// var companyStatus = $('#editCompaniestatus').val();

					if(companyName == "") {
						$("#editCompanyName").after('<p class="text-danger">Company Name field is required</p>');
						$('#editCompanyName').closest('.form-group').addClass('has-error');
					} else {
						// remov error text field
						$("#editCompanyName").find('.text-danger').remove();
						// success out for form 
						$("#editCompanyName").closest('.form-group').addClass('has-success');	  	
					}

					// if(companyStatus == "") {
					// 	$("#editCompaniestatus").after('<p class="text-danger">Company Name field is required</p>');

					// 	$('#editCompaniestatus').closest('.form-group').addClass('has-error');
					// } else {
					// 	// remove error text field
					// 	$("#editCompaniestatus").find('.text-danger').remove();
					// 	// success out for form 
					// 	$("#editCompaniestatus").closest('.form-group').addClass('has-success');	  	
					// }

					if(companyName) {
						var form = $(this);

						// submit btn
						$('#editCompanyBtn').button('loading');

						$.ajax({
							url: form.attr('action'),
							type: form.attr('method'),
							data: form.serialize(),
							dataType: 'json',
							success:function(response) {

								if(response.success == true) {
									console.log(response);
									// submit btn
									$('#editCompanyBtn').button('reset');

									// reload the manage member table 
									manageCompanyTable.ajax.reload(null, false);								  	  										
									// remove the error text
									$(".text-danger").remove();
									// remove the form error
									$('.form-group').removeClass('has-error').removeClass('has-success');
			  	  			
			  	  			$('#edit-company-messages').html('<div class="alert alert-success">'+
			            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
			            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
			          '</div>');

			  	  			$(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									}); // /.alert
								} // /if
									
							}// /success
						});	 // /ajax												
					} // /if

					return false;
				}); // /update company form

			} // /success
		}); // ajax function

	} else {
		alert('error!! Refresh the page again');
	}
} // /edit Companies function

function removeCompanies(companyId = null) {
	// alert('jack')
	if(companyId) {
		$('#removeCompanyId').remove();
		$.ajax({
			url: 'php_action/fetchSelectedCompany.php',
			type: 'post',
			data: {companyId : companyId},
			dataType: 'json',
			success:function(response) {
				$('.removeCompanyFooter').after('<input type="hidden" name="removeCompanyId" id="removeCompanyId" value="'+response.company_id+'" /> ');

				// click on remove button to remove the company
				$("#removeCompanyBtn").unbind('click').bind('click', function() {
					// button loading
					$("#removeCompanyBtn").button('loading');

					$.ajax({
						url: 'php_action/removeCompany.php',
						type: 'post',
						data: {companyId : companyId},
						dataType: 'json',
						success:function(response) {
							console.log(response);
							// button loading
							$("#removeCompanyBtn").button('reset');
							if(response.success == true) {

								// hide the remove modal 
								$('#removeMemberModal').modal('hide');

								// reload the company table 
								manageCompanyTable.ajax.reload(null, false);
								
								$('.remove-messages').html('<div class="alert alert-success">'+
			            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
			            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
			          '</div>');

			  	  			$(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									}); // /.alert
							} else {

							} // /else
						} // /response messages
					}); // /ajax function to remove the company

				}); // /click on remove button to remove the company

			} // /success
		}); // /ajax

		$('.removeCompanyFooter').after();
	} else {
		alert('error!! Refresh the page again');
	}
} // /remove Companies function

function addUser(companyId) {
	// submit user form function
	$("#submitUserForm_" + companyId).unbind('submit').bind('submit', function(e) {
		e.preventDefault();
		// remove the error text
		$(".text-danger").remove();
		// remove the form error
		$('.form-group').removeClass('has-error').removeClass('has-success');			

		// var company_id = $("#company_id_" + companyId).val(companyId);
		var firstName = $("#firstName_" + companyId).val();
		var lastName = $("#lastName_" + companyId).val();
		var userName = $("#userName_" + companyId).val();

		if(firstName == "") {
			$("#firstName_" + companyId).after('<p class="text-danger">First Name field is required</p>');
			$("#firstName_" + companyId).closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#firstName_" + companyId).find('.text-danger').remove();
			// success out for form 
			$("#firstName_" + companyId).closest('.form-group').addClass('has-success');	  	
		}

		if(lastName == "") {
			$("#lastName_" + companyId).after('<p class="text-danger">Last Name field is required</p>');
			$('#lastName_' + companyId).closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#lastName_" + companyId).find('.text-danger').remove();
			// success out for form 
			$("#lastName_" + companyId).closest('.form-group').addClass('has-success');	  	
		}

		if(userName == "") {
			$("#userName_" + companyId).after('<p class="text-danger">User Name field is required</p>');
			$('#userName_' + companyId).closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#userName_" + companyId).find('.text-danger').remove();
			// success out for form 
			$("#userName_" + companyId).closest('.form-group').addClass('has-success');	  	
		}

		if(firstName && lastName && userName) {
			var form = $(this);
			// button loading
			$("#createUserBtn_" + companyId).button('loading');

			$.ajax({
				url : form.attr('action'),
				type: form.attr('method'),
				data: form.serialize(),
				dataType: 'json',
				success:function(response) {
					// button loading
					$("#createUserBtn_" + companyId).button('reset');

					if(response.success == true) {
						// reload the manage member table 
						manageCompanyTable.ajax.reload(null, false);						

  	  			// reset the form text
						$("#submitUserForm_" + companyId)[0].reset();
						// remove the error text
						$(".text-danger").remove();
						// remove the form error
						$('.form-group').removeClass('has-error').removeClass('has-success');
  	  			
  	  			$('#add-user-messages_' + companyId).html('<div class="alert alert-success">'+
            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
          '</div>');

  	  			$(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					}  // if

				} // /success
			}); // /ajax	
		} // if

		return false;
	}); // /submit user form function
}